variable "runner_token" {
  type = string
}

variable "gitlab_url" {
  type = string
}

variable "name" {
  type = string
}

