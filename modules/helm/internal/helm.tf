resource "helm_release" "gitlab-runner" {
  name       = var.name
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"
  set {
    name  = "gitlabUrl"
    value = var.gitlab_url
  }
  set {
    name  = "rbac.create"
    value = "true"
  }
  set {
    name  = "runnerToken"
    value = var.runner_token
  }
}
