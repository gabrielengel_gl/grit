output "id" {
  value = module.vpc.id
}

output "subnet_id" {
  value = module.vpc.subnet_id
}
