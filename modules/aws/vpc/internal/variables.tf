variable "labels" {
  type = map(any)
}

variable "name" {
  type = string
}

variable "zone" {
  type = string
}

variable "cidr" {
  type = string
}

variable "subnet_cidr" {
  type = string
}
