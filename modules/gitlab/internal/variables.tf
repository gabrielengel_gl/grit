variable "url" {
  type = string
}

variable "project_id" {
  type = string
}

variable "runner_description" {
  type = string
}

variable "runner_tags" {
  type = list(string)
}

variable "name" {
  type = string
}
