#######################
# METADATA VALIDATION #
#######################

module "validate-name" {
  source = "../../../internal/validation/name"
  name   = var.metadata.name
}

module "validate-support" {
  source   = "../../../internal/validation/support"
  use_case = "runner"
  use_case_support = tomap({
    "runner" = "experimental"
  })
  min_support = var.metadata.min_support
}

######################
# RUNNER PROD CONFIG #
######################

module "runner" {
  source = "../internal"

  name   = var.metadata.name
  labels = var.metadata.labels

  google_project = var.google_project
  google_zone    = var.google_zone

  service_account_email = var.service_account_email
  machine_type          = var.machine_type
  disk_type             = var.disk_type
  disk_size_gb          = var.disk_size_gb
  runner_version        = var.runner_version

  concurrent     = var.concurrent
  check_interval = var.check_interval
  log_level      = var.log_level
  listen_address = var.listen_address

  gitlab_url          = var.gitlab_url
  runner_token        = var.runner_token
  request_concurrency = var.request_concurrency
  executor            = var.executor

  cache_gcs_bucket = var.cache_gcs_bucket

  runners_global_section = var.runners_global_section
  runners_docker_section = var.runners_docker_section

  fleeting_googlecompute_plugin_version = var.fleeting_googlecompute_plugin_version
  fleeting_instance_group_name          = var.fleeting_instance_group_name

  capacity_per_instance = var.capacity_per_instance
  max_use_count         = var.max_use_count
  max_instances         = var.max_instances
  autoscaling_policies  = var.autoscaling_policies

  vpc = var.vpc
}
